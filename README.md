# Bash scripting best practices, tips and tricks

**Pipeline status (based on Shellcheck)**

[![pipeline status](https://gitlab.com/pchevallereau/bash-scripting-guide/badges/master/pipeline.svg)](https://gitlab.com/pchevallereau/bash-scripting-guide/-/commits/master)

## Websites to know
- [Advanced Bash Scripting (ABS)](https://www.tldp.org/LDP/abs/html/)
- [opensource.com](https://opensource.com/)
- [Shellcheck](https://www.shellcheck.net/)

## Summary
0. [Best practices](#best-practices)
    - [Shebang](#shebang)
    - [Banner](#banner)
    - [Invoque subshell](#invoque-subshell)

1. [Basics](#basics)
    - [Variables](#variables)
    - [Bash Arrays](#bash-arrays)

2. [Tips](#tips)
    - [Parsing command-line arguments](#parsing-command-line-arguments)
    - [Perform curl request and check it](#perform-curl-request-and-check-it)
    - [Define and call function](#define-and-call-function)
    - [Prevent concurrency executions of a script](#prevent-concurrency-executions-of-a-script)

3. [Tricks](#tricks)
    - [Catching errors like a pro](#catching-errors-like-a-pro)

## Basics

### Shebang
The shebang is always the first line of the script, it define the interpreter which will be used when the script execution

`#!/usr/bin/env bash`

#### Banner
The banner is cool for external users, for example they can check and compare their version with the main repository
```bash
##############################################################
#       script-name.sh
#       Version 1.0.0 - 12/04/2019
#       Author : YOUR NAME
#       Mail: YOUR MAIL
#       Quick script description
##############################################################
```

#### Invoque subshell
Prefer use this syntax `"$(cmdToExec)"` instead backquote
```bash
$ var="$(date)"
$ echo "${var}"
samedi 11 mai 2019, 11:36:03 (UTC+0200)
```

## Best practices

#### Variables
Assignment :
```bash
HOSTNAME='test1' # prefer uppercase for environment and global variables
val='1' && valList='1,2' # prefer lowercase and camelCase for local variables
```
 
Call :
```bash
echo "${var}" # as possible, suround variables like this.
```

Manipulate :
```bash
##############################################################
#       Substitution
##############################################################

# You can substitute characters with variables, here is an example we need to replace the ',' by a space :
$ val='toto,toto2'
$ val="${val//,/ }"
$ echo "${val}"
toto toto2

##############################################################
#       Default value if empty
##############################################################

# If variable is empty, she are populated with the default value (This is usefull on script, you can easily detect errors) :

# Example with empty variable
$ defaultValue='ERROR'
$ val=''
$ val="${val:-$defaultValue}"
$ echo "${val}"
ERROR # The variable was populated with default value because she was empty

# Example with populated variable
$ defaultValue='ERROR'
$ val='toto'
$ val="${val:-$defaultValue}"
$ echo "${val}"
toto # The variable was not populated with default value because she was non-empty
```

#### Bash arrays
```bash
# Create array :
array=''

# Add one or more items to array :
array=( "${array[@]}" 'A' 'b' 'C' 'd' )

# Delete one or more items to array ( !! start from 0 to n ) :
unset array[1,3] # 'b' and 'd' were delete but the rest of items wasn't reindex, for example 'C' still exist on position 2

# List all items of array : 
echo ${array[@]}

# Count items of array ( !! if using this in for loop, don't forget to start from 0 to n )
echo ${#array[@]}
```

## Tips

#### Parsing command-line arguments
Wiki :
- "${#}" = number of passed arguments
- "${@}" = list of all arguments

Code :
```bash
#!/usr/bin/env bash
if [[ "${#}" -gt 0  ]] ; then   
  for opts in "${@}"; do
    case "${opts}" in
      --silent|-s)        # Argument without value { long | short }
      ;;
      --user=*|-u=*)      # Argument with value {long | short}
        user=${opts//*=}
        echo "$user"
      ;;
      *)                  # Match everything if not found before
        echo "error : ${opts}"
      ;;
    esac
  done
fi
```

How to use :
1. Write code in test.sh
2. Make it executable `chmod +x test.sh`
3. Run
```bash
$ ./test.sh --user=TOTO
TOTO
```

#### Perform curl request and check it
I use this code to perform and check my curl request, you can use it in function mode
<!-- blank line -->
Don't forget '-w http_code=%{http_code}' in curl arguments
<!-- blank line -->
Content are stored in ${curlBody}

Code :
```bash
#!/usr/bin/env bash
url="https://opensource.com"
curlExec="$(curl -sw "http_code=%{http_code}" ${url})"
curlReturn="${?}"
curlHttpCode=$(echo "${curlExec}" | grep -o 'http_code=.*$')
curlBody=${curlExec//${curlHttpCode}/}

if [[ "${curlReturn}" == "0" ]] && [[ "${curlHttpCode//*=}" == "200" ]] ; then
  echo 'OK'
else
  echo 'NOPE'
fi

echo "${curlBody}"
```

How to use :
1. Write code in test.sh
2. Make it executable `chmod +x test.sh`
3. Run
```bash
$ ./test.sh
OK
--output-truncate--
```


#### Define and call function
I like named my function with an underscore at start, because this increase functions identification and code comprehension.

Code :
```bash
#!/usr/bin/env bash

# Define function
_helloWorld()
{
  echo 'Hello World'
}

# Call function
_helloWorld
```

How to use :
1. Write code in test.sh
2. Make it executable `chmod +x test.sh`
3. Run
```bash
$ ./test.sh
Hello World
```

#### Prevent concurrency executions of a script
Code :
```bash
#!/usr/bin/env bash

baseName=${0##*/}
basePID=${$}

# Check if another process of this script exist
if [[ -f "/var/run/${baseName}.pid" ]] ; then
  if pgrep -F "/var/run/${baseName}.pid" ; then
    echo "another instance of ${baseName} is already running"
    exit
  else
    echo "${basePID}" > "/var/run/${baseName}.pid"
  fi
else
  echo "${basePID}" > "/var/run/${baseName}.pid"
fi

sleep 100
```

How to use :
1. Write code in test.sh
2. Make it executable `chmod +x test.sh`
3. Run
```bash
$ ./test.sh &
$ ./test.sh
another instance of test.sh is already running
```

## Tricks

#### Catch errors like a pro

Use [bash-errorsTracker](https://gitlab.com/pchevallereau/bash-errorstracker)
